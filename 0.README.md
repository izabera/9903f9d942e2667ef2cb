This is the table from `man 5 terminfo`.

Do NOT hardcode terminal escape sequences. Use tput with the cap-names from the table below to get the right code for your terminal. 

<table>
  <tr>
    <td width="10%">(P)</td>
    <td>indicates that padding may be specified</td>
  </tr>
  <tr>
    <td width="10%">#[1-9]</td>
    <td>in the description field indicates that the string is passed through tparm with parms as given (#i).</td>
  </tr>
  <tr>
    <td width="10%">(P*)</td>
    <td>indicates that padding may vary in proportion to the number of lines affected</td>
  </tr>
  <tr>
    <td width="10%">(#i)</td>
    <td>indicates the ith parameter.</td>
  </tr>
</table>